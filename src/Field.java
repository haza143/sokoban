import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import javax.swing.*;

public class Field extends JFrame{
	private final int maxLevel = 4;
	Robot r;
	Scanner scan;
	Level level;
	CanvasPanel canvasPanel;
	String title;
	int steps;
	private ArrayList<char[][]> undo = new ArrayList<char[][]>();
	int currentLevel;
	JLabel label;
	Field() throws Exception{
		steps = 0;
		currentLevel = 0;
		title = "Level #" + currentLevel;
		scan = new Scanner (new File("level"+ currentLevel + ".txt"));
		level = new Level(scan);
		r = new Robot(level.getRobotRow(), level.getRobotCol());
		canvasPanel = new CanvasPanel(r, level);
		canvasPanel.setFocusable(true);
		
		add(canvasPanel, BorderLayout.CENTER);
		canvasPanel.addKeyListener(new PanelKeyListener());
		char [][] maze = new char [level.getHeight()][level.getWidth()];
		for (int i = 0; i < level.getHeight(); i++){
			for (int j = 0; j < level.getWidth(); j++){
				maze[i][j] = level.getChar(i, j);
			}
		}
		setSize(level.getWidth()*100, level.getHeight()*100);
		undo.add(maze);
		label = new JLabel("Your steps: " + steps);
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setVerticalTextPosition(SwingConstants.TOP);
		label.setSize(50, 10);
		add(label, BorderLayout.NORTH);
		scan.close();

	}
	
	public static void main(String[] args) throws Exception {
		Field frame = new Field(); 
		
		frame.setTitle(frame.title);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		
		
	}
		
	
	
	public void nextLevel () throws Exception{
		
		if (currentLevel == maxLevel)
			currentLevel = -1;
		
		currentLevel++;
		title = "Level #" + currentLevel;
		setTitle(title);
		scan = new Scanner (new File("level"+ currentLevel + ".txt"));
		level.change(scan);
		setSize(level.getWidth()*100, level.getHeight()*100);
		r.change(level);
		undo = new ArrayList<char[][]>();
		char [][] maze = new char [level.getHeight()][level.getWidth()];
		for (int i = 0; i < level.getHeight(); i++){
			for (int j = 0; j < level.getWidth(); j++){
				maze[i][j] = level.getChar(i, j);
			}
		}
		
		undo.add(maze);
		steps = 0;
		scan.close();

		
	}

	
			
	private class PanelKeyListener extends KeyAdapter{
		void add (){
			char [][] maze = new char [level.getHeight()][level.getWidth()];
			for (int i = 0; i < level.getHeight(); i++){
				for (int j = 0; j < level.getWidth(); j++){
					maze[i][j] = level.getChar(i, j);
				}
			}
			
			undo.add(maze);
			steps++;
		}
		@Override
		public void keyPressed(KeyEvent e) {
			switch(e.getKeyCode()){
			case KeyEvent.VK_LEFT:
				if (level.moveLeft(r.getRow(), r.getCol() - 1))
				{
					r.moveLeft();
					add();
				}
				break;
			case KeyEvent.VK_UP:
				if (level.moveUp(r.getRow() - 1, r.getCol()))
				{
					r.moveUp();
					add();
				}
				break;
			case KeyEvent.VK_RIGHT:
				if (level.moveRight(r.getRow(), r.getCol() + 1))
				{
					r.moveRight();
					add();
				}
				break;
			case KeyEvent.VK_DOWN:
				if (level.moveDown(r.getRow() + 1, r.getCol()))
				{
					r.moveDown();
					add();						
				}
				break;
			case KeyEvent.VK_U:
				if (undo.size() > 1){
					level.setMaze(undo.get(undo.size() - 2));
					undo.remove(undo.size() - 1);
				}
				r.moveBack();
				if (steps > 0)
					steps--;
				break;
			case KeyEvent.VK_F1:
				JOptionPane.showMessageDialog(null, info);
				break;
				
				
			}
			
			repaint();
			label.setText("Your steps: " + steps);
			if (level.win()){
				int a = JOptionPane.YES_NO_CANCEL_OPTION;
				
				if (JOptionPane.showConfirmDialog(null, "You are the winner!\nYour steps: " + steps + "\nBest Steps: " + level.getBestSteps() +  "\nDo you want to play next level?", "", a)
				 == JOptionPane.YES_OPTION){
					try {
						nextLevel();
						repaint();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
				
			}
			
			
		}
		
	} 

	String info = "U - undo\nF1 - info\nArrows - moves\nSokoban game\n" + "Created by Azamat Derkenbaev\n";
	
}
